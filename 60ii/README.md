PC-6001mkII  BIOS and System ROMs
=================================

Contents:
- `BASICROM.62`: [nk]
- `CGROM60.62`:  [nk]
- `CGROM60m.62`: [nk]
- `KANJIROM.62`: [nk]
- `VOICEROM.62`: [nk]

ROM image sources inculude:
- \[nk]: [Neo Kobe - NEC PC-6001 (2016-02-25)][nk6001] (archive.org)
  - Includes two BIOS ROM archives with identical contents:
    `[BIOS] PC-6001mkII (62) [ROM].7z` and
    `[BIOS] PC-6001mkII (62) [ROM] [b].7z`.



<!-------------------------------------------------------------------->
[nk6001]: https://archive.org/details/Neo_Kobe_NEC_PC-6001_2016-02-25
