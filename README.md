PC-6001 Series Reverse Engineering
=====================================

ROM images:
- `60/`: PC-6001 (1981-11)
- `60A/`: PC-6001A (198?, "NEC TREK" North American version)
- `60ii/`: PC-6001mkII (1983-07)
- `66/`: PC-6601 ROM (1983-11)
- `60sr/`: PC-6001mkII (1984-11) SR
- `60iisr/`: PC-6001mkIISR (1984-11)
- `66sr/`: PC-6601SR ROM (1984-11)

ROM image sources inculude:
- [Neo Kobe - NEC PC-6001 (2016-02-25)][nk6001] (archive.org)

The BASIC modes offered by the various machines are:
- Mode 1 = PC-6001 (1-2 pages)
- Mode 2 = PC-6001 with ROM/RAM expansion cartridge (1-4 pages)
- Mode 3 = PC-6001 in Extended BASIC (1-2 pages)
- Mode 4 = PC-6001 with expansion cartridge in Extended BASIC (1-4 pages)
- Mode 5 = N60m BASIC (mkII) or N66 BASIC (PC-6601/SR) (1-4 pages)
- Mode 6 = N66 BASIC (2 pages automatically)

The required mode is often specified by software in the filename, e.g.,
`[m2p2]` for Mode 2, 2 pages of VRAM.



<!-------------------------------------------------------------------->
[nk6001]: https://archive.org/details/Neo_Kobe_NEC_PC-6001_2016-02-25

