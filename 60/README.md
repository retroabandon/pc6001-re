PC-6001 BIOS and System ROMs
============================

Contents:
- `BASICROM.60`:  [nk `[BIOS] PC-6001 (60) [ROM].7z`]
- `CGROM60.60`:   [nk `[BIOS] PC-6001 (60) [ROM].7z`]
- `BASICROM.60b`: [nk `[BIOS] PC-6001 (60) [ROM] [b].7z`, added `b` to name]
- `CGROM60.60b`:  [nk `[BIOS] PC-6001 (60) [ROM] [b].7z`, added `b` to name]
- `CGROM60M.60`:  [nk `[BIOS] PC-6001 (60) [ROM] [VDG Font ROM].7z`]
- `CGROM60S.60`:  [nk `[BIOS] PC-6001 (60) [ROM] [VDG Font ROM].7z`]
- `CGROM60S.60a`: [nk `[BIOS] PC-6001 (60) [ROM] [VDG Font ROM].7z`,
                      renamed from `alts/CGROM60S.60`]

ROM image sources inculude:
- \[nk]: [Neo Kobe - NEC PC-6001 (2016-02-25)][nk6001] (archive.org)



<!-------------------------------------------------------------------->
[nk6001]: https://archive.org/details/Neo_Kobe_NEC_PC-6001_2016-02-25
